from cProfile import run
from struct import pack_into
from tqdm import tqdm
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
import torchvision.transforms as transforms
import tenseal as ts
from medmnist import INFO, Evaluator
from medmnist.utils import montage2d

## Encryption Parameters
def create_keys(bits_scale = 26):
    # controls precision of the fractional part
    bits_scale = bits_scale

    # Create TenSEAL context
    context = ts.context(
        ts.SCHEME_TYPE.CKKS,
        poly_modulus_degree=8192,
        coeff_mod_bit_sizes=[31, bits_scale, bits_scale, bits_scale, bits_scale, bits_scale, bits_scale, 31]
    )

    # set the scale
    context.global_scale = pow(2, bits_scale)

    # galois keys are required to do ciphertext rotations
    context.generate_galois_keys()
    return context
    
class EncConvNet:
    def __init__(self, torch_nn):
        self.conv1_weight = torch_nn.layer1[0].weight.data.view(
            torch_nn.layer1[0].out_channels, torch_nn.layer1[0].kernel_size[0],
            torch_nn.layer1[0].kernel_size[1]
        ).tolist()
        self.conv1_bias = torch_nn.layer1[0].bias.data.tolist()
        
        self.fc1_weight = torch_nn.fc[0].weight.T.data.tolist()
        self.fc1_bias = torch_nn.fc[0].bias.data.tolist()
        
        self.fc2_weight = torch_nn.fc[2].weight.T.data.tolist()
        self.fc2_bias = torch_nn.fc[2].bias.data.tolist()
        
        
    def forward(self, enc_x, windows_nb):
        # conv layer
        enc_channels = []
        for kernel, bias in zip(self.conv1_weight, self.conv1_bias):
            y = enc_x.conv2d_im2col(kernel, windows_nb) + bias
            enc_channels.append(y)
        enc_x = ts.CKKSVector.pack_vectors(enc_channels)
        # pack all channels into a single flattened vector
        # # square activation
        enc_x.square_()
        # # fc1 layer
        enc_x = enc_x.mm(self.fc1_weight) + self.fc1_bias
        # # square activation
        enc_x.square_()
        # # fc2 layer
        enc_x = enc_x.mm(self.fc2_weight) + self.fc2_bias
        return enc_x
    
    def __call__(self, *args, **kwargs):
        return self.forward(*args, **kwargs)

def encrypted_model(model):
    return EncConvNet(model)

def enc_test(context, model, enc_model, test_loader, criterion, kernel_shape, stride, n_classes, task, run_to_ten = True, split = 'test'):
    # initialize lists to monitor test loss and accuracy
    
    enc_model.eval()
    class_correct = list(0. for i in range(n_classes))
    class_total = list(0. for i in range(n_classes))
    
    class_correct_true = list(0. for i in range(n_classes))
    class_total_true = list(0. for i in range(n_classes))
    count = 1
    count_true = 1
    with torch.no_grad():
        for data, target in tqdm(test_loader):
            if count % 100 == 0 and run_to_ten == True:
                break
            for i in range(2):

                outputs = model(data)

                if task == 'multi-label, binary-class':
                    targets = target.to(torch.float32)
                    outputs = outputs.softmax(dim=-1)
                    # print(outputs)
                    # break
                else:
                    targets = target.squeeze().long()
                    outputs = outputs.softmax(dim=-1)
                    targets = target.float()#.resize_(len(targets), 1)


                outputs = torch.tensor(outputs).view(1, -1)

                # convert output probabilities to predicted class
                _, pred_true = torch.max(outputs, 1)
                # compare predictions to true label
                correct_true = np.squeeze(pred_true.eq(target.data.view_as(pred_true)))
                # calculate test accuracy for each object class
                label = target.data[0]
                print(pred_true)
    #             class_correct_true[label] += correct_true.item()
    #             class_total_true[label] += 1
    #             count_true +=1

            
            
        
    #         print(data.shape,target)
            # Encoding and encryption
        for i in range(2):

            x_enc, windows_nb = ts.im2col_encoding(
                context, data.view(28, 28).tolist(), kernel_shape[0],
                kernel_shape[1], stride
            )


            # Encrypted evaluation
            enc_output = enc_model(x_enc, windows_nb)
            # Decryption of result
            output = enc_output.decrypt()
            output = torch.tensor(output).view(1, -1)


            # convert output probabilities to predicted class
            _, pred = torch.max(output, 1)
            print(pred)
            # compare predictions to true label
            correct = np.squeeze(pred.eq(target.data.view_as(pred)))
            # calculate test accuracy for each object class
            label = target.data[0]
            class_correct[label] += correct.item()
            class_total[label] += 1
            count +=1
        print('pred')        
        print('===========')
    
    print(f'Unencrypted model found: {class_correct_true}')    
    print(f'Encrypted model found:   {class_correct}')    
    print(f'True Values:             {class_total}')
    for label in range(n_classes):
        print(
            f'Test Accuracy of True {label}: {int(100 * class_correct_true[label] / class_total_true[label])}% '
            f'({int(np.sum(class_correct_true[label]))}/{int(np.sum(class_total_true[label]))})'
        )

    print(
        f'\nTest Accuracy True (Overall): {int(100 * np.sum(class_correct_true) / np.sum(class_total_true))}% ' 
        f'({int(np.sum(class_correct_true))}/{int(np.sum(class_total_true))})'
    )

    for label in range(n_classes):
        print(
            f'Test Accuracy of {label}: {int(100 * class_correct[label] / class_total[label])}% '
            f'({int(np.sum(class_correct[label]))}/{int(np.sum(class_total[label]))})'
        )

    print(
        f'\nTest Accuracy (Overall): {int(100 * np.sum(class_correct) / np.sum(class_total))}% ' 
        f'({int(np.sum(class_correct))}/{int(np.sum(class_total))})'
    )