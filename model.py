from cProfile import run
from struct import pack_into
from tqdm import tqdm
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
import torchvision.transforms as transforms
import tenseal as ts

import medmnist
from medmnist import INFO, Evaluator
from medmnist.utils import montage2d


# data_flag = 'pathmnist'
data_flag = 'breastmnist'
download = True

NUM_EPOCHS = 25
BATCH_SIZE = 128
lr = 0.001

info = INFO[data_flag]
task = info['task']
n_channels = info['n_channels']
n_classes = len(info['label'])

DataClass = getattr(medmnist, info['python_class'])

def import_data(data_flag = 'breastmnist', download = True, BATCH_SIZE = 128):
    # preprocessing
    data_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[.5], std=[.5])
    ])
    info = INFO[data_flag]
    DataClass = getattr(medmnist, info['python_class'])

    # load the data
    train_dataset = DataClass(split='train', transform=data_transform, download=download)
    test_dataset = DataClass(split='test', transform=data_transform, download=download)

    pil_dataset = DataClass(split='train', download=download)

    # encapsulate data into dataloader form
    train_loader = data.DataLoader(dataset=train_dataset, batch_size=BATCH_SIZE, shuffle=True)
    train_loader_at_eval = data.DataLoader(dataset=train_dataset, batch_size=2*BATCH_SIZE, shuffle=False)
    test_loader = data.DataLoader(dataset=test_dataset, batch_size=2*BATCH_SIZE, shuffle=False)
    train_loader_at_eval_enc = data.DataLoader(dataset=train_dataset, batch_size=1, shuffle=False)
    test_loader_enc = data.DataLoader(dataset=test_dataset, batch_size=1, shuffle=False)
    return train_dataset, test_dataset, train_loader, train_loader_at_eval, test_loader, train_loader_at_eval_enc, test_loader_enc

    # define a simple CNN model
class Square(nn.Module):
    def forward(self, x):
        return torch.square(x)

class Net(nn.Module):
    def __init__(self, in_channels, num_classes):
        super(Net, self).__init__()

        self.layer1 = nn.Sequential(
            nn.Conv2d(in_channels, 16, kernel_size=7, stride = 3),
            # nn.BatchNorm2d(16),
            Square(),)

        # self.layer2 = nn.Sequential(
        #     nn.Conv2d(16, 16, kernel_size=3),
        #     nn.BatchNorm2d(16),
        #     Square(),
        #     nn.MaxPool2d(kernel_size=2, stride=2))

        # self.layer3 = nn.Sequential(
        #     nn.Conv2d(16, 64, kernel_size=3),
        #     nn.BatchNorm2d(64),
        #     Square())
        
        # self.layer4 = nn.Sequential(
        #     nn.Conv2d(64, 64, kernel_size=3),
        #     nn.BatchNorm2d(64),
        #     Square())

        # self.layer5 = nn.Sequential(
        #     nn.Conv2d(64, 64, kernel_size=3, padding=1),
        #     nn.BatchNorm2d(64),
        #     Square(),
        #     nn.MaxPool2d(kernel_size=2, stride=2))

        self.fc = nn.Sequential(
            nn.Linear(16 * 8 * 8 , 128),
            Square(),
            # nn.Linear(128, 128),
            # Square(),
            nn.Linear(128, num_classes))

    def forward(self, x):
        # print(f'input is {x.shape}')
        x = self.layer1(x)
        # print(f'layer1 is {x.shape}')
        # x = self.layer2(x)
        # print(f'layer2 is {x.shape}')
        # x = self.layer3(x)
        # print(f'layer3 is {x.shape}')
        # x = self.layer4(x)
        # print(f'layer4 is {x.shape}')
        # x = self.layer5(x)
        # print(f'layer5 is {x.shape}')
        x = x.view(x.size(0), -1)
        # print(f'reshape is {x.shape}')
        x = self.fc(x)
        # print(f'fc is {x.shape}')
        return x

def new_model(n_channels, n_classes, task, lr):

    model = Net(in_channels=n_channels, num_classes=n_classes)
        
    # define loss function and optimizer
    if task == "multi-label, binary-class":
        criterion = nn.BCEWithLogitsLoss()
    else:
        criterion = nn.CrossEntropyLoss()
        
    optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9)
    return model, optimizer, criterion


def model_training(model, optimizer, criterion, train_loader, NUM_EPOCHS, task):
    # train

    for epoch in range(NUM_EPOCHS):
        train_correct = 0
        train_total = 0
        test_correct = 0
        test_total = 0
        
        model.train()
        for inputs, targets in tqdm(train_loader):
            # forward + backward + optimize
            optimizer.zero_grad()
            outputs = model(inputs)
            # break
            
            if task == 'multi-label, binary-class':
                targets = targets.to(torch.float32)
                loss = criterion(outputs, targets)
            else:
                targets = targets.squeeze().long()
                loss = criterion(outputs, targets)
            
            loss.backward()
            optimizer.step()

    
    return model, optimizer, criterion


def save_model(model, path):
    torch.save(model, path)

def load_model(path):
    model = torch.load(path)
    model.eval()
    return model

    
def test_model(split, model, data_split, task, data_flag):
    model.eval()
    y_true = torch.tensor([])
    y_score = torch.tensor([])
    
    data_loader = data_split

    with torch.no_grad():
        for inputs, targets in data_loader:

            outputs = model(inputs)

            if task == 'multi-label, binary-class':
                targets = targets.to(torch.float32)
                outputs = outputs.softmax(dim=-1)
                # print(outputs)
                # break
            else:
                targets = targets.squeeze().long()
                outputs = outputs.softmax(dim=-1)
                targets = targets.float().resize_(len(targets), 1)

            y_true = torch.cat((y_true, targets), 0)
            y_score = torch.cat((y_score, outputs), 0)

        y_true = y_true.numpy()
        y_score = y_score.detach().numpy()
        # print(y_score)
        
        evaluator = Evaluator(data_flag, split)
        metrics = evaluator.evaluate(y_score)
    
        print('%s  auc: %.3f  acc:%.3f' % (split, *metrics))
    return y_true, y_score, evaluator, metrics
        
## Encryption Parameters
def create_keys(bits_scale = 26):
    # controls precision of the fractional part
    bits_scale = bits_scale

    # Create TenSEAL context
    context = ts.context(
        ts.SCHEME_TYPE.CKKS,
        poly_modulus_degree=8192,
        coeff_mod_bit_sizes=[31, bits_scale, bits_scale, bits_scale, bits_scale, bits_scale, bits_scale, 31]
    )

    # set the scale
    context.global_scale = pow(2, bits_scale)

    # galois keys are required to do ciphertext rotations
    context.generate_galois_keys()
    return context

    
class EncConvNet:
    def __init__(self, torch_nn):
        self.conv1_weight = torch_nn.layer1[0].weight.data.view(
            torch_nn.layer1[0].out_channels, torch_nn.layer1[0].kernel_size[0],
            torch_nn.layer1[0].kernel_size[1]
        ).tolist()
        self.conv1_bias = torch_nn.layer1[0].bias.data.tolist()
        
        self.fc1_weight = torch_nn.fc[0].weight.T.data.tolist()
        self.fc1_bias = torch_nn.fc[0].bias.data.tolist()
        
        self.fc2_weight = torch_nn.fc[2].weight.T.data.tolist()
        self.fc2_bias = torch_nn.fc[2].bias.data.tolist()
        
        
    def forward(self, enc_x, windows_nb):
        # conv layer
        enc_channels = []
        for kernel, bias in zip(self.conv1_weight, self.conv1_bias):
            y = enc_x.conv2d_im2col(kernel, windows_nb) + bias
            enc_channels.append(y)
        print(y)
        print(enc_channels)
        print("======")
        enc_x = ts.CKKSVector.pack_vectors(enc_channels)
        print(enc_x)
        # pack all channels into a single flattened vector
        # # square activation
        enc_x.square_()
        # # fc1 layer
        enc_x = enc_x.mm(self.fc1_weight) + self.fc1_bias
        # # square activation
        enc_x.square_()
        # # fc2 layer
        enc_x = enc_x.mm(self.fc2_weight) + self.fc2_bias
        return enc_x
    
    def __call__(self, *args, **kwargs):
        return self.forward(*args, **kwargs)

def enc_test(context, enc_model, test_loader, criterion, kernel_shape, stride, run_to_ten = True):
    # initialize lists to monitor test loss and accuracy
    test_loss = 0.0
    class_correct = list(0. for i in range(10))
    class_total = list(0. for i in range(10))
    count = 1
    for data, target in tqdm(test_loader):
        print(data.shape,target)
        if count % 10 == 0 and run_to_ten == True:
            print(count)
            break
        # Encoding and encryption
        x_enc, windows_nb = ts.im2col_encoding(
            context, data.view(28, 28).tolist(), kernel_shape[0],
            kernel_shape[1], stride
        )
        
        
        # Encrypted evaluation
        enc_output = enc_model(x_enc, windows_nb)
        # Decryption of result
        output = enc_output.decrypt()
        output = torch.tensor(output).view(1, -1)

        
        # convert output probabilities to predicted class
        _, pred = torch.max(output, 1)
        # compare predictions to true label
        correct = np.squeeze(pred.eq(target.data.view_as(pred)))
        # calculate test accuracy for each object class
        label = target.data[0]
        class_correct[label] += correct.item()
        class_total[label] += 1
        count +=1


    for label in range(2):
        print(
            f'Test Accuracy of {label}: {int(100 * class_correct[label] / class_total[label])}% '
            f'({int(np.sum(class_correct[label]))}/{int(np.sum(class_total[label]))})'
        )

    print(
        f'\nTest Accuracy (Overall): {int(100 * np.sum(class_correct) / np.sum(class_total))}% ' 
        f'({int(np.sum(class_correct))}/{int(np.sum(class_total))})'
    )

def encrypted_model(model):
    return EncConvNet(model)


def encrypt_one(model, context, data_split, image):
    kernel_shape = model.layer1[0].kernel_size
    stride = model.layer1[0].stride[0]
    enc_model = encrypted_model(model)

    split = 'test'
    data_loader = data_split

    for inputs, targets in data_loader:
        x_enc, windows_nb = ts.im2col_encoding(
                    context, inputs.view(28, 28).tolist(), kernel_shape[0],
                    kernel_shape[1], stride
                )
        targets = targets.squeeze().long()
        # targets = targets.float().resize_(len(targets), 1)
        outputs_enc = enc_model(x_enc, windows_nb)
        
        outputs = model(inputs)
        break

    return outputs, outputs_enc, targets


train_dataset, test_dataset, train_loader, train_loader_at_eval, test_loader, train_loader_at_eval_enc, test_loader_enc = import_data()


print(test_loader_enc)
print("===================")
print(train_loader_at_eval_enc)

# model, optimizer, criterion = new_model(n_channels, n_classes, task, lr)

def random_subset(loader, size):
    random_ids = np.random.randint(len(loader), size=size)
    print(random_ids)
    new_subset = loader.sampler([1])
    print(len(new_subset))
    print(new_subset)
    return new_subset


def run_for_one(image, split):
    path = f'models/{data_flag}_model.pth'

    # save_model(model, path)

    model2 = load_model(path)

    print("=======")
    print(model2)



    data_split = split

    image = 25



    context = create_keys()
    outputs, enc_outputs, labels = encrypt_one(model2, context, data_split, image)

    outputs = torch.tensor(outputs).softmax(dim=-1)
    enc_outputs = torch.tensor(enc_outputs.decrypt()).softmax(dim=-1)
    labels = labels.item()

    _, pred_outputs = torch.max(torch.tensor(outputs).view(1, -1), 1)
    _, pred_enc_outputs = torch.max(torch.tensor(enc_outputs).view(1, -1), 1)

    print("outputs======")
    print(torch.tensor(outputs).softmax(dim=-1))
    print(pred_outputs)
    print("encoutputs======")
    print(torch.tensor(enc_outputs).softmax(dim=-1))
    print(pred_enc_outputs)

    print("======")
    print(labels)

    return pred_outputs, pred_enc_outputs

# run_for_one(5)

random_subset(test_loader_enc, 15)