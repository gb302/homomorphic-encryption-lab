aniso8601==9.0.1
attrs==21.4.0
certifi==2022.5.18.1
charset-normalizer==2.0.12
click==8.1.3
colorama==0.4.4
fire==0.4.0
Flask==2.1.2
Flask-Cors==3.0.10
flask-restx==0.5.1
idna==3.3
imageio==2.19.3
importlib-metadata==4.13.0
importlib-resources==5.7.1
itsdangerous==2.1.2
Jinja2==3.1.2
joblib==1.1.1
jsonschema==4.6.0
MarkupSafe==2.1.1
medmnist==2.1.0
networkx==2.8.2
numpy==1.23.0
packaging==21.3
pandas==1.5.3
Pillow==9.1.1
pyparsing==3.0.9
pyrsistent==0.18.1
python-dateutil==2.8.2
pytz==2022.5
PyWavelets==1.3.0
requests==2.31.0
scikit-image==0.19.2
scikit-learn==1.1.1
scipy==1.8.1
six==1.16.0
tenseal==0.3.14
termcolor==1.1.0
threadpoolctl==3.1.0
tifffile==2022.5.4
torch==1.11.0
torchaudio==0.11.0
torchvision==0.12.0
tqdm==4.64.0
typing_extensions==4.2.0
urllib3==1.26.9
Werkzeug==2.1.2
zipp==3.8.0
matplotlib==3.6.0
waitress==2.1.2
