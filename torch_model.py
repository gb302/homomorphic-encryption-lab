from cProfile import run
from struct import pack_into
from tqdm import tqdm
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
import torchvision.transforms as transforms
import tenseal as ts

import matplotlib.pyplot as plt
import medmnist
from medmnist import INFO, Evaluator
from medmnist.utils import montage2d


class Square(nn.Module):
    def forward(self, x):
        return torch.square(x)


class Net(nn.Module):
    def __init__(self, in_channels, num_classes):
        super(Net, self).__init__()

        self.layer1 = nn.Sequential(
            nn.Conv2d(in_channels, 16, kernel_size=7, stride = 3),
            # nn.BatchNorm2d(16),
            Square())

#         self.layer2 = nn.Sequential(
#             nn.Conv2d(16, 16, kernel_size=3),
#         #     nn.BatchNorm2d(16),
#             Square(),
        #     nn.MaxPool2d(kernel_size=2, stride=2))

        # self.layer3 = nn.Sequential(
        #     nn.Conv2d(16, 64, kernel_size=3),
        #     nn.BatchNorm2d(64),
        #     Square())
        
        # self.layer4 = nn.Sequential(
        #     nn.Conv2d(64, 64, kernel_size=3),
        #     nn.BatchNorm2d(64),
        #     Square())

        # self.layer5 = nn.Sequential(
        #     nn.Conv2d(64, 64, kernel_size=3, padding=1),
        #     nn.BatchNorm2d(64),
        #     Square(),
        #     nn.MaxPool2d(kernel_size=2, stride=2))

        self.fc = nn.Sequential(
            nn.Linear(16 * 8 * 8 , 128),
            Square(),
            # nn.Linear(128, 128),
            # Square(),
            nn.Linear(128, num_classes))

    def forward(self, x):
        # print(f'input is {x.shape}')
        x = self.layer1(x)
        # print(f'layer1 is {x.shape}')
#         x = self.layer2(x)
        # print(f'layer2 is {x.shape}')
        # x = self.layer3(x)
        # print(f'layer3 is {x.shape}')
        # x = self.layer4(x)
        # print(f'layer4 is {x.shape}')
        # x = self.layer5(x)
        # print(f'layer5 is {x.shape}')
        x = x.view(x.size(0), -1)
        # print(f'reshape is {x.shape}')
        x = self.fc(x)
        # print(f'fc is {x.shape}')
        return x



def model_training(model, optimizer, criterion, train_loader, valid_loader, NUM_EPOCHS, task):
    # trainabs
    train_accu = []
    total_train_loss = []
    valid_accu = []
    total_valid_loss = []
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
#     device = "cpu"

    for epoch in tqdm(range(NUM_EPOCHS)):



        train_loss = 0.0
        valid_loss = 0.0    

        correct_train = 0
        total_train = 0     

        correct_valid = 0
        total_valid = 0
        
        
        model.train()
        for inputs, targets in (train_loader):
            # forward + backward + optimize
            optimizer.zero_grad()
            outputs = model(inputs).to(device)
# #             print(outputs[:10])
# #             print(outputs.softmax(dim=-1)[:10])
#             for i in targets:
#                 if i == 2:
#                     print('hit')
#             print(targets[:100])
#             break
            # break
            
            if task == 'multi-label, binary-class':
                targets = targets.to(torch.float32)
                loss = criterion(outputs, targets.to(device))
            else:
                targets = targets.squeeze().long()
                loss = criterion(outputs, targets.to(device))
            
            loss.backward()
            optimizer.step()
            
            train_loss += loss.item()*inputs.size(0)

            #calculate accuracy
            _, predicted = outputs.cpu().max(1)
            total_train += targets.cpu().size(0)
            correct_train += predicted.eq(targets).sum().item()
        

        model.eval() # prep model for evaluation

        with torch.no_grad():
            for inputs, targets in (valid_loader):
                # forward pass: compute predicted outputs by passing inputs to the model
                outputs = model(inputs).to(device)
                # calculate the loss
            
                if task == 'multi-label, binary-class':
                    targets = targets.to(torch.float32)
                    loss = criterion(outputs, targets.to(device))
                    outputs = outputs.softmax(dim=-1)
                else:
                    targets = targets.squeeze().long()
                    outputs = outputs.softmax(dim=-1)
#                     print(targets)
                    loss = criterion(outputs, targets.to(device))
                    
                # update running validation loss 
                valid_loss += loss.item()*inputs.size(0)

                _, predicted = outputs.cpu().max(1)
                total_valid += targets.cpu().size(0)
                correct_valid += predicted.eq(targets).sum().item()
            
        train_loss = train_loss/len(train_loader.sampler)
        valid_loss = valid_loss/len(valid_loader.sampler)   

        accu_train=100.*correct_train/total_train
        accu_valid=100.*correct_valid/total_valid
        
        total_train_loss.append(train_loss)

        train_accu.append(accu_train)

        valid_accu.append(accu_valid)
        total_valid_loss.append(valid_loss)
        
        print('Train Loss: %.3f'%(train_loss))  
        print('Valid Loss: %.3f'%(valid_loss))
        print('Train Accuracy: %.3f'%(accu_train))  
        print('Valid Accuracy: %.3f'%(accu_valid))

    plt.plot(train_accu,'-o')
    plt.plot(valid_accu,'-o')
    plt.xlabel('epoch')
    plt.ylabel('accuracy')
    plt.legend(['Train','Valid'])
    plt.title('Train vs Valid Accuracy')
    plt.show()

    
    plt.plot(total_train_loss,'-o')
    plt.plot(total_valid_loss,'-o')
    plt.xlabel('epoch')
    plt.ylabel('losses')
    plt.legend(['Train','Valid'])
    plt.title('Train vs Valid Losses')
    plt.show()
    return model, optimizer, criterion

def test_model(split, model, data_split, task, data_flag):
    model.eval()
    y_true = torch.tensor([])
    y_score = torch.tensor([])
    
    data_loader = data_split
    print(task,data_flag)
    with torch.no_grad():
        for inputs, targets in data_loader:

            outputs = model(inputs)
#             print(outputs)

            if task == 'multi-label, binary-class':
                targets = targets.to(torch.float32)
                outputs = outputs.softmax(dim=-1)
                # break
            else:
                targets = targets.squeeze().long()
                outputs = outputs.softmax(dim=-1)
                targets = targets.float().resize_(len(targets), 1)
                
                
            y_true = torch.cat((y_true, targets), 0)
            y_score = torch.cat((y_score, outputs), 0)
            
        y_true = y_true.numpy()
        y_score = y_score.numpy()
        
        evaluator = Evaluator(data_flag, split)
        metrics = evaluator.evaluate(y_score)
    
        print('%s  auc: %.3f  acc:%.3f' % (split, *metrics))
    return y_true, y_score, evaluator, metrics

def save_model(model, path):
    torch.save(model, path)


def load_model(path, in_channels, num_classes):
    model = Net(in_channels, num_classes)
    model = torch.load(path)
    model.eval()
    return model
