from cProfile import run
from struct import pack_into
from tqdm import tqdm
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
import torchvision.transforms as transforms
import tenseal as ts
from skimage.util import montage as skimage_montage
from PIL import Image
import base64
from io import BytesIO
import medmnist
from medmnist import INFO, Evaluator
from medmnist.utils import montage2d
from torch_model import Net
import torchvision.transforms.functional as TF


def import_data(data_flag = 'breastmnist', download = False, BATCH_SIZE = 128):
    # preprocessing
    data_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[.5], std=[.5])
    ])
    info = INFO[data_flag]
    DataClass = getattr(medmnist, info['python_class'])

    # load the data
    train_dataset = DataClass(split='train', transform=data_transform, download=download)
    valid_dataset = DataClass(split='val', transform=data_transform, download=download)
    test_dataset = DataClass(split='test', transform=data_transform, download=download)

    pil_dataset = DataClass(split='train', download=download)

    # encapsulate data into dataloader form
    train_loader = data.DataLoader(dataset=train_dataset, batch_size=BATCH_SIZE, shuffle=True)
    train_loader_at_eval = data.DataLoader(dataset=valid_dataset, batch_size=2*BATCH_SIZE, shuffle=False)
    
    
    test_loader = data.DataLoader(dataset=test_dataset, batch_size=2*BATCH_SIZE, shuffle=False)
    
    train_loader_at_eval_enc = data.DataLoader(dataset=valid_dataset, batch_size=1, shuffle=False)
    test_loader_enc = data.DataLoader(dataset=test_dataset, batch_size=1, shuffle=False)
    
    
    return train_dataset, valid_dataset, test_dataset, train_loader, train_loader_at_eval, test_loader, train_loader_at_eval_enc, test_loader_enc

def new_model(n_channels, n_classes, task, lr):

    model = Net(in_channels=n_channels, num_classes=n_classes)
        
    # define loss function and optimizer
    if task == "multi-label, binary-class":
        criterion = nn.BCEWithLogitsLoss()
    else:
        criterion = nn.CrossEntropyLoss()
        
#     optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9)
    optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=1e-5)
    return model, optimizer, criterion

def send_image_id_pair(length = 1, dataset = None):
    img_id_pair = { 'img_array':[]}
    # 'idx' : [], 'img_frame':[],
    n_sel = length * length
    sel = np.random.choice(dataset.__len__(), size=n_sel, replace=False)
    for i, val in enumerate(sel):
        image = montage2d(dataset.imgs, 1, [val])
        
        buffered = BytesIO()
        image.save(buffered, format="JPEG")
        img_str = base64.b64encode(buffered.getvalue())
        
        img_id_pair['idx'].append(val)
        img_id_pair['img_frame'].append(img_str)
        img_id_pair['img_array'].append()
    
    return img_id_pair



def montage2d(imgs, n_channels, sel):

    sel_img = imgs[sel]
    montage_arr = skimage_montage(sel_img, multichannel=(n_channels == 3))
    montage_img = Image.fromarray(montage_arr)

    return montage_img

def enc_one_all_server(context, model, enc_model, image_id, criterion, kernel_shape, stride, dataset, task):
    # initialize lists to monitor test loss and accuracy
    
    model.eval()
    with torch.no_grad():
        data = torch.unsqueeze(dataset[image_id][0],0)
        targets = torch.tensor(dataset[image_id][1])

        outputs_unenc = model(data)

        if task == 'multi-label, binary-class':
            targets = targets.to(torch.float32)
            outputs_unenc = outputs_unenc.softmax(dim=-1)
            # print(outputs)
            # break
        else:
            targets = targets.squeeze().long()
            outputs_unenc = outputs_unenc.softmax(dim=-1)
            targets = targets.float()#.resize_(len(targets), 1)


        outputs_unenc = outputs_unenc.clone().detach().view(1, -1)

                # convert output probabilities to predicted class
        _, pred_true = torch.max(outputs_unenc, 1)
        print(pred_true)

            
            
        
    #         print(data.shape,target)
            # Encoding and encryption

    x_enc, windows_nb = ts.im2col_encoding(
        context, data.view(28, 28).tolist(), kernel_shape[0],
        kernel_shape[1], stride
    )


    # Encrypted evaluation
    enc_output = enc_model(x_enc, windows_nb)
    # Decryption of result
    output = enc_output.decrypt()
    output = output.clone().detach().view(1, -1)


    # convert output probabilities to predicted class
    _, pred_enc = torch.max(output, 1)
    print(pred_enc)
    print(targets)
    
    return pred_true, pred_enc, targets

def encrypt_one(context, data, kernel_shape, stride):
    x_enc, windows_nb = ts.im2col_encoding(
        context, data.view(28, 28).tolist(), kernel_shape[0],
        kernel_shape[1], stride
    )
    return x_enc, windows_nb

def decrypt_one(context, data):
    output =  ts.ckks_vector_from(context, data).decrypt()
    return output

def load_image(image):
    data_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[.5], std=[.5])
    ])
    img = Image.open(image)
    x = data_transform(img)
    data = torch.unsqueeze(x,0)
    return data


def encrypt_creation_run_unencrypt(context, model, image_id, kernel_shape, stride, dataset, task):
    # initialize lists to monitor test loss and accuracy
    data_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[.5], std=[.5])
    ])
    img = Image.open(f'images/OrganAMNIST/{image_id}.jpeg')
    img.save("test.jpg")
    x = data_transform(img)
    # x.unsqueeze_(0)
    # print(img)
    # print("=======================")
    # print(dataset[image_id])
    model.eval()
    with torch.no_grad():
        # data = torch.unsqueeze(dataset[image_id][0],0)
        data = torch.unsqueeze(x,0)
        print(data)
        # print(data)
        # data = torch.unsqueeze(dataset[image_id][0],0)
        targets = torch.tensor(dataset[image_id][1])

        outputs_unenc = model(data)

        if task == 'multi-label, binary-class':
            targets = targets.to(torch.float32)
            outputs_unenc = outputs_unenc.softmax(dim=-1)
            # print(outputs)
            # break
        else:
            targets = targets.squeeze().long()
            outputs_unenc = outputs_unenc.softmax(dim=-1)
            targets = targets.float()#.resize_(len(targets), 1)


        outputs_unenc = outputs_unenc.clone().detach().view(1, -1)

                # convert output probabilities to predicted class
        _, pred_true = torch.max(outputs_unenc, 1)
            

    x_enc, windows_nb = encrypt_one(context, data, kernel_shape, stride)


    
    return pred_true, targets, x_enc, windows_nb
    
def encrypted_inference(enc_model, data, windows_nb):
    
    # Encrypted evaluation
    enc_output = enc_model(data, windows_nb)

    
    return enc_output
    
def decrypt_inference(context, data, torch_val, target, info):
    
    unenc_data = decrypt_one(context, data)
    output = torch.tensor(unenc_data).view(1, -1)
    _, pred_enc = torch.max(output, 1)
    pred_enc = info["label"][str(pred_enc.item())]
    torch_val =info["label"][str(torch_val.item())]
    target = info["label"][str(int(target.item()))]
    answer_dict = {'pred':pred_enc, 'valid':torch_val, 'target': target}
    return answer_dict